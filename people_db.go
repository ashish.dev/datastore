package main

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type Person struct {
	Name string `bson:"name" json:"name"`
	Age  int32  `bson:"age" json:"age"`
}

func GetPeople(mongoDb string) []Person {

	// Set client options
	//"mongodb://192.168.0.254:27017"
	clientOptions := options.Client().ApplyURI(mongoDb)

	// Connect to MongoDB
	client, e := mongo.Connect(context.TODO(), clientOptions)
	CheckError(e)

	// Check the connection
	e = client.Ping(context.TODO(), readpref.Primary())
	CheckError(e)

	// get collection as ref
	collection := client.Database("testdb1").Collection("people1")

	// // insert
	// john := Person{"John", 24}
	// jane := Person{"Jane", 27}
	// ben := Person{"Ben", 16}

	// _, e = collection.InsertOne(context.TODO(), john)
	// CheckError(e)

	// persons := []interface{}{jane, ben}
	// _, e = collection.InsertMany(context.TODO(), persons)
	// CheckError(e)

	// // update
	filter := bson.D{{}}

	// update := bson.D{
	// 	{"$set", bson.D{
	// 		{"age", 26},
	// 	}},
	// }

	// _, e = collection.UpdateOne(context.TODO(), filter, update)
	// CheckError(e)

	// find
	// var res Person
	// e = collection.FindOne(context.TODO(), filter).Decode(&res)
	res, e := collection.Find(context.TODO(), filter)
	fmt.Println("Printing Result")
	fmt.Println(res)
	var resAll []Person
	res.All(context.TODO(), &resAll)
	fmt.Print(resAll)
	return resAll
	// delete
	// _, e = collection.DeleteMany(context.TODO(), bson.D{{}})
	// CheckError(e)
}

func CheckError(e error) {
	if e != nil {
		fmt.Println("Printing Error")
		fmt.Println(e)
	}
}
