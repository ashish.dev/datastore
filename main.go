package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"

	pb "adesigner.in/ashish/app-db/proto"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type Server struct {
	pb.UnimplementedAccountServiceServer
}

func (s *Server) Get(ctx context.Context, request *pb.GetAccountRequest) (*pb.GetAccountResponse, error) {
	log.Printf("inside get Request %v", ctx.Value("Auth"))
	headers, err := metadata.FromIncomingContext(ctx)
	log.Printf("failed to listen: %v", err)

	log.Printf("Header Auth %s", headers.Get("Authorization")[0])

	return &pb.GetAccountResponse{
		Code:    200,
		Message: "Success",
		Items: []*pb.Account{
			{
				AccountId: 1,
				FirstName: "Asd",
				LastName:  "asdasd",
				Dob:       100000,
			},
		},
	}, nil
}

func (s *Server) GetAllPerson(ctx context.Context, e *empty.Empty) (*pb.GetAllPersonResponse, error) {
	log.Printf("inside Get All Person Request")
	mongoDb := os.Getenv("MONGO_URI")
	peoples := GetPeople(mongoDb)
	var grpcPeoples []*pb.Person
	for _, people := range peoples {
		grpcPeoples = append(grpcPeoples, &pb.Person{
			Name: people.Name,
			Age:  people.Age,
		})
	}
	return &pb.GetAllPersonResponse{
		Code:    200,
		Message: "All Person",
		Items:   grpcPeoples,
	}, nil
}

func main() {
	port, hasPort := os.LookupEnv("GRPC_PORT")
	_, hasMongoUri := os.LookupEnv("MONGO_URI")
	if hasPort == false {
		log.Fatal("Port Required to initiate GRPC server `GRPC_PORT`")
	}
	if hasMongoUri == false {
		log.Fatal("Mongo URI Required to initiate GRPC server `MONGO_URI`")
	}
	lis, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%s", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	pb.RegisterAccountServiceServer(s, &Server{})
	log.Printf("server listening at %v", lis.Addr())

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
